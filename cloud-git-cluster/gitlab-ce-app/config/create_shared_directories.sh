#!/bin/sh -
#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#

#
# set up shared data
#
if mkdir /var/opt/gitlab/.shared_data_mutex
then
    #
    # create directory structure
    #
    mkdir -p /var/opt/gitlab/.ssh
    mkdir -p /var/opt/gitlab/backups
    mkdir -p /var/opt/gitlab/git-data/repositories
    mkdir -p /var/opt/gitlab/gitlab-rails/etc
    mkdir -p /var/opt/gitlab/gitlab-rails/uploads
    mkdir -p /var/opt/gitlab/gitlab-rails/working
    cp -a shared /var/opt/gitlab/gitlab-rails/shared
    mkdir -p /var/opt/gitlab/gitlab-ci/builds
    chown -R git:git /var/opt/gitlab
    chmod -R ug+rwX,o-rwx,ug-s /var/opt/gitlab/git-data/repositories
    chmod 700 /var/opt/gitlab/gitlab-rails/uploads
    find /var/opt/gitlab/git-data/repositories -type d -exec chmod g+s {} \;
    if [ ! -d public/uploads/. ]
    then
        ln -s /var/opt/gitlab/gitlab-rails/uploads public/uploads
    fi
    if [ ! -d public/working/. ]
    then
        ln -s /var/opt/gitlab/gitlab-rails/working public/working
    fi
    mkdir /var/opt/gitlab/.shared_data_initialized
fi

while [ ! -d /var/opt/gitlab/.shared_data_initialized ]
do
    sleep 1
done
