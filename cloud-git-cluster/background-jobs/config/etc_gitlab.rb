#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

external_url "#{ENV['CLOUD_GIT_SCHEME']}://#{ENV['CLOUD_GIT_SUBDOMAIN']}.#{ENV['CLOUD_GIT_DOMAIN']}#{ENV['CLOUD_GIT_URL_ROOT']}"

gitlab_rails['https'] = "https".eql? ENV['CLOUD_GIT_SCHEME']

bootstrap['enable'] = false
geo_postgresql['enable'] = false
gitaly['enable'] = false
gitlab_monitor['enable'] = true
gitlab_pages['enable'] = false
gitlab_workhorse['enable'] = false
logrotate['enable'] = true
mailroom['enable'] = false
manage_accounts['enable'] = true
manage_storage_directories['enable'] = true
mattermost['enable'] = false
mattermost_nginx['enable'] = false
nginx['enable'] = false
node_exporter['enable'] = false
pages_nginx['enable'] = false
postgres_exporter['enable'] = false
postgresql['enable'] = false
prometheus['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
redis_exporter['enable'] = false
redis_master_role['enable'] = false
redis_sentinel_role['enable'] = false
redis_slave_role['enable'] = false
registry['enable'] = false
registry_nginx['enable'] = false
sentinel['enable'] = false
sidekiq['enable'] = true
sidekiq_cluster['enable'] = false
unicorn['enable'] = false
## setting to disable gitlab_shell ?

gitlab_rails['gitlab_email_enabled'] = true
gitlab_rails['gitlab_email_from'] = "#{ENV['CLOUD_GIT_EMAIL_FROM']}"
gitlab_rails['gitlab_email_display_name'] = "#{ENV['CLOUD_GIT_EMAIL_DISPLAY_NAME']}"
gitlab_rails['gitlab_email_reply_to'] = "#{ENV['CLOUD_GIT_EMAIL_REPLY_TO']}"
gitlab_rails['gitlab_email_subject_suffix'] = "#{ENV['CLOUD_GIT_EMAIL_SUBJECT_SUFFIX']}"

gitlab_rails['db_adapter'] = "postgresql"
gitlab_rails['db_encoding'] = "unicode"
gitlab_rails['db_collation'] = nil
gitlab_rails['db_database'] = "gitlabhq_production"
gitlab_rails['db_pool'] = 10
gitlab_rails['db_username'] = "gitlab"
gitlab_rails['db_password'] = "gitlab"
gitlab_rails['db_host'] = "cloud-git-postgresql"
gitlab_rails['db_port'] = 5432
gitlab_rails['db_socket'] = nil
gitlab_rails['db_sslmode'] = nil
gitlab_rails['db_sslrootcert'] = nil
gitlab_rails['db_prepared_statements'] = true
gitlab_rails['db_statements_limit'] = 1000

#### Redis TCP connection
gitlab_rails['redis_host'] = "cloud-git-redis"
gitlab_rails['redis_port'] = 6379
gitlab_rails['redis_password'] = nil
gitlab_rails['redis_database'] = 0

#### Enable or disable automatic database migrations
gitlab_rails['auto_migrate'] = false

### Advanced settings - used by gitlab_shell client
unicorn['listen'] = "cloud-git-gitlab-ce-app"
unicorn['port'] = 8080

gitlab_rails['internal_api_url'] = "http://cloud-git-gitlab-ce-app:8080"
